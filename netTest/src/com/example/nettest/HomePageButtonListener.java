package com.example.nettest;

import android.view.View;

public class HomePageButtonListener implements View.OnClickListener {
	
	private MainActivity mBase;
	
	public HomePageButtonListener(MainActivity base) {
		this.mBase = base;
	}
	
	@Override
	public void onClick(View v) {
		this.mBase.doLogin();
	}

}
