package com.example.nettest.returnable;

import java.util.Map;

import android.content.Context;

import com.saguarodigital.returnable.IParser;
import com.saguarodigital.returnable.IReturnable;
import com.saguarodigital.returnable.IReturnableCache;
import com.saguarodigital.returnable.annotation.Field;
import com.saguarodigital.returnable.annotation.Type;
import com.saguarodigital.returnable.defaultimpl.AutoParser;
import com.saguarodigital.returnable.defaultimpl.EmptyCache;

@Type(version = 1, base = "user_info")
public class UserSession implements IReturnable {
	@Field(type = Field.Type.NUMBER, constraint = Field.Constraint.NOT_NULL, json_name = "user_id")  public int mID;
	@Field(type = Field.Type.TEXT, constraint = Field.Constraint.NOT_NULL)    public String mEmail;
	@Field(type = Field.Type.TEXT, constraint = Field.Constraint.NOT_NULL, json_name = "user_auth_expire_time")    public String mSessionExpires;
	@Field(type = Field.Type.TEXT, constraint = Field.Constraint.NOT_NULL, json_name = "user_auth_hash")    public String mSessionID;
	
	
	@Override
	public IReturnableCache<UserSession> getCache(Context arg0) {
		return new EmptyCache<UserSession>();
	}
	@Override
	public IParser<UserSession> getParser() {
		return new AutoParser<UserSession>();
	}
	@Override
	public String getUri(Map<String, String> arg0) {
		return "http://billing.mikandi.com/v1/user/login?username=chris007&password=abc123";
	}



}


