package com.example.nettest;

import com.example.nettest.returnable.UserSession;
import com.saguarodigital.returnable.defaultimpl.JSONAsyncTask;
import com.saguarodigital.returnable.defaultimpl.JSONResponse;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}

		final Button button = (Button) findViewById(R.id.buttonButton);
		button.setOnClickListener(new HomePageButtonListener(this));

	}

	
	public void doLogin() {
		
		final EditText editText = (EditText) findViewById(R.id.username_field);
		final EditText editText2 = (EditText) findViewById(R.id.password_field);
		
		String usernameString = editText.getText().toString();
		String passwordString = editText2.getText().toString();

		Log.i("username", usernameString);
		Log.i("password", passwordString);
		
		this.mLogInTask.execute();
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private JSONAsyncTask<UserSession> mLogInTask = new JSONAsyncTask<UserSession>
	(
			this,
			UserSession.class,
			"http://billing.mikandi.com/v1/user/login?username=chris007&password=abc123"
	){
		@Override
		protected void onPostExecute(JSONResponse<UserSession> result) {
			super.onPostExecute(result);
	
			if (result != null) {
				Log.i("ResponseCode: ", "" + result.getCode()); 
			} else {
				Log.i("ERROR:", "Response is Null");
			}
		}
	};
}
